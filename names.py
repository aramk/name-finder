import re
import modules.futil as futil
import modules.dutil as dutil
import modules.putil as putil
import modules.pmatch as pmatch
from modules.ngram import *
from resources.titlemap import *

class LookupResource:
	resource = None
	name = None
	filename = None
	def __init__(self, resource, filename, name=None):
		self.resource = resource
		self.filename = filename
		if name == None:
			name = filename
		self.name = name

class Name:
	def __repr__(self):
		if not self.parent:
			return str(vars(self))
		else:
			return '#' + str(self.id)
	
	def __init__(self):
		self.score = \
		self.id = \
		self.prefix = \
		self.title = \
		self.initials = \
		self.parts = \
		self.plural = \
		self.children = \
		self.parent = \
		self.suffix = None
	
	def toStr(self):
		return str(vars(self))
		
	def toFullName(self, nf):
		title = ''
		parts = ''
		initials = ''
		if self.title:
			for i in range(len(self.title)):
				t = self.title[i]
				if nf.isAbbrTitle(t):
					self.title[i] += '.'
			title = " ".join(self.title)
			if len(title) > 0:
				title += ' '
		if self.parts:
			for i in self.parts:
				if len(i) == 1:
					parts += i
					parts += '. '
				else:
					parts += i + ' '
			parts = parts.strip()
		if self.initials:
			initials = ". ".join(self.initials)
			if len(initials) > 0:
				initials += '. '
		return title + initials + parts
	
	def childList(self):
		c = self.children
		nc = []
		if c and len(c) > 0:
			for i in c:
				nc.append(i.id)
		return nc

class NameFinder:
	# Regex
	RE_NAME = r"(?:\b(?:(?![0-9_])\w')?(?![A-Z]+\b)(?![a-z]+\b)(?:(?![0-9_])\w)+\b)"	
	RE_WORD = r"(?:\b(?:(?:(?![A-Z0-9_\s]).)+)\b)"
	RE_SEP = r"(?:[\r\n]{2}|[^\s\w])[ \t]*"
	
	# Regex at runtime
	RE_TITLE = RE_TITLE_NAMES = RE_NAMES_FULL = None
	
	# Paths
	RESOURCE_PATH = 'resources/'
	
	# Text to search
	_text = None
	_resources = {}
	_matches = []

	def __init__(self):
		# Load resources
		self.addResource('words')
		self.addResource('locations')
		self.addResource('countries')
		self.addResource('particles')
		self.addResource('buildings')
		self.addResource('business')
		self.addResource('bad_suffix')
		self.addResource('bad_prefix')
		self.addResource('titles')
		self.addResource('abbr_titles')
		self._initRegex()
	
	def _initRegex(self):
		# Generate runtime regex
		abbr_titles = dutil.capList(dutil.setToList(self.getResource('abbr_titles')))
		particles = dutil.capList(dutil.setToList(self.getResource('particles')))
		self.RE_NAMES = r"(?m)((?:\b[A-Z](?:\.\s*|[ \t]*[\s][ \t]*))+)?(%s(?:(?:(?:[ \t]*[\s][ \t]*(?:(?:\"?%s\"?)|(?:%s)|(?:[A-Z]\.?)))*)[ \t]*[\s][ \t]*%s)?)" % (self.RE_NAME, self.RE_NAME, r"|".join(particles), self.RE_NAME) # John, John Smith
		self.RE_TITLE = r"\b(?:%s)\b" % r"|".join(abbr_titles)
		self.RE_TITLE_NAMES = r"(?:(%s)\.?[ \t]*[\s][ \t]*)?%s" % (self.RE_TITLE, self.RE_NAMES)
		self.RE_NAMES_FULL = r"(?:((?:%s[ \t]*[\s][ \t]*)|%s))?%s(?:%s)?(?:(?:(?:[ \t]*[\s][ \t]*|%s)(%s)))?" % (self.RE_WORD, self.RE_SEP, self.RE_TITLE_NAMES, r"((?:'[a-z])|(?:(?<=[a-z])'))", self.RE_SEP, self.RE_WORD) #, self.RE_WORD, self.RE_NAME)
	
	def load(self, filename):
		self._text = futil.read(filename, utf8=True)
	
	def text(self, value = None):
		if value is None:
			return self._text
		else:
			self._text = value
	
	def addResource(self, filename, name=None):
		resource = LookupResource(self.getLines(filename), filename, name)
		self._resources[resource.name] = resource
	
	def getResourceObj(self, name):
		if name in self._resources:
			return self._resources[name]
		else:
			return None
			
	def getResource(self, name):
		r = self.getResourceObj(name)
		if r:
			return r.resource
		else:
			return None
	
	def loadCheck(self):
		if self._text is None:
			raise Exception("Nothing loaded")
		 
	def getLines(self, filename, cap=False, lower=False, strip=True):
		lines = futil.lines(self.RESOURCE_PATH + filename)
		if cap:
			caps = dutil.capList(lines)
			lines.extend(caps)
		if lower:
			lines = [x.lower() for x in lines]
		if strip:
			lines = [x.strip() for x in lines]
		return dutil.listToSet(lines)
	
	def sub(self, lst, flags=None):
		text = self._text
		if (len(lst) > 0):
			for i in lst:
				text = re.sub(r"\b%s\b" % i, '', text, 0, flags)
		return text
	
	def hasSuffix(self, word, suffix):
		return word[-len(suffix):] == suffix
	
	def subSuffix(self, word, a, b):
		if self.hasSuffix(word, a):
			return word[:-len(a)] + b
		else:
			return word
	
	def resourceLookup(self, i, name, lower=True):
		if lower:
			i = i.lower()
		r = self.getResourceObj(name)
		if not r:
			return None
		else:
			return i in r.resource
	
	def isWord(self, i):
		return self.resourceLookup(i, 'words')
		
	def isBroadWord(self, i):
		return self.isWord(i) or self.isCountry(i) or self.isLocation(i) or self.isPlural(i)
		
	def isLocation(self, i):
		return self.resourceLookup(i, 'locations')
		
	def isCountry(self, i):
		return self.resourceLookup(i, 'countries')
	
	def isParticle(self, i):
		return self.resourceLookup(i, 'particles')
		
	def isAbbrTitle(self, i):
		return self.resourceLookup(i, 'abbr_titles')
		
	def isTitle(self, i):
		return self.resourceLookup(i, 'titles')

	def isBusiness(self, i):
		return self.resourceLookup(i, 'business')
		
	def isBuilding(self, i):
		return self.resourceLookup(i, 'buildings')
	
	def isPlural(self, word):
		ip = False
		if len(word) > 2:
			if word[-1] == 's':
				ip = self.isWord(word[:-1])
			else:
				ip = self.isWord(word+'s')
		if not ip and len(word) > len('men') and self.hasSuffix(word, 'men'):
			ip = self.isWord(self.subSuffix(word, 'men', 'man'))
		return ip
	
	def isSeperator(self, i):
		return i is not None and re.match(r'^'+self.RE_SEP+r'$', i) != None
	
	def areMatchingNames(self, q, t, debug=False):
		isTitleMatch = True
		match = None
		
		if q.title and t.title:
			# If both titles given, must match.
			# TODO this means Mr Watson and Dr Watson are not equal here
			# NOTE: If more than one, like "Lance Corporal", then only last needs to match
			qt = dutil.dictValueOrDefault(q.title[-1].lower(), titleMap, q.title[-1].lower())
			tt = dutil.dictValueOrDefault(t.title[-1].lower(), titleMap, t.title[-1].lower())
			isTitleMatch = qt == tt
			if debug:
				print 'titles:',qt,tt
		
		if isTitleMatch:
			qp = q.parts
			tp = t.parts
			if q.initials:
				qp = q.initials + q.parts
			if t.initials:
				tp = t.initials + t.parts
			
			if len(qp) > len(tp):
				# Ensure query is always shortest for partial matching
				tmp = q
				q = t
				t = tmp
				tmp = qp
				qp = tp
				tp = tmp
			
			if debug:
				print 'q:', qp
				print 't:', tp
			
			# Partially match parts
			if len(qp) == 1:
				# Ignore if it's a word and not equal to each other
				if len(tp) == 1:
						# Jeff and Jeffery
					if self.isWord(qp[0]) or self.isWord(tp[0]) or (q.title and not t.title) or (t.title and not q.title):
						# Prevents 'The' and 'Then' or 'Theo' grouping
						if qp[0] == tp[0]:
							match = tp
					else:	
						pm = pmatch.partialMatchStrings(qp[0],tp[0])
						if pm:
							match = [pm]
				elif not self.isWord(qp[0]):
					# if self.isWord(qp[0]):
						# Don't allow "The" and "The New York Orchestra" to partially match
					if qp[0] == tp[-1]:
						# Skilling and Jeffery Skilling
						match = tp
					else:
						# Jeff and Jeffery Skilling
						# NOTE: can't match Jeff and J Skilling, not enough info
						# Therefore, query MUST exist partially in target, not other way
						if len(tp[0]) >= len(qp[0]) and tp[0].lower().find(qp[0].lower()) == 0:
							match = tp
			elif qp[-1] == tp[-1]:
				# Jeff Skilling and Jeffery K. Skilling
				match = pmatch.partialMatchStringLists(qp[:-1],tp[:-1])
				if match:
					match.append(tp[-1])
			
		if debug:
			print 'match:',match
			putil.dashes()
			
		
		return match
	
	def areMatchingNamesNgram(self, q, t, debug=False):
		match = False
		
		if q.title and t.title:
			# If both titles given, must match.
			qt = dutil.dictValueOrDefault(q.title.lower(), titleMap, q.title.lower())
			tt = dutil.dictValueOrDefault(t.title.lower(), titleMap, t.title.lower())
			match = qt == tt
			if debug:
				print 'titles:',qt,tt
		
		if match: # TODO bug here, won't go through unless titles same
			if len(q.parts) == len(t.parts):
				match = q.parts == t.parts
			else:
				if len(q.parts) > len(t.parts):
					# Ensure query is always shortest for partial matching
					tmp = q
					q = t
					t = tmp
		
				qgrams = NGramWords(q.parts)
				tgrams = NGramWords(t.parts)
			
				# First and last names must match
				match = qgrams.first == tgrams.first and qgrams.last == tgrams.last
				if debug:
					print 'first and last ngrams:'
					print qgrams.first,qgrams.last
					print tgrams.first,tgrams.last
				if match:
					# Score must also be >= len(q)
					score = qgrams.compare(tgrams)
					match = score >= len(q.parts)
					if debug:
						print 'score:',score
		
		if debug:
			print 'match:', match
		
		return False
	
	def newGroupNames(self, a, b):
		# Find most complete info in q and t
		# NOTE: At this point, any intials partially matched are grouped in front of "parts"
		# So we only need to worry about grabbing the title
		g = Name()
		g.id = a.id + 1 # Arbitrary
		g.title = a.title if a.title != None else b.title
		g.score = a.score + b.score
		if g.title and g.score < 0:
			g.score = 0
		g.children = [a,b]
		a.parent = g
		b.parent = g
		return g
		
	def mergeNames(self, a, b):
		a.id = b.id + 1 # Arbitrary
		a.title = a.title if a.title != None else b.title
		a.score += b.score
		if a.title and a.score < 0:
			a.score = 0
		a.children.append(b)
		b.parent = a
	
	def groupedNames(self, ns, debug=False):
		'''
		ns is a list of names to group with partial matching
		'''
		if len(ns) <= 1:
			return ns
			
		index = 0
		while index < len(ns):
			# First item is query, rest are target names to try matching
			q = ns[index]
			if q is None:
				index += 1
				continue
			if debug:
				putil.dashes()
				print 'query:', q
				putil.dashes()
			for i in range(index+1,len(ns)):
				t = ns[i]
				if t is None:
					continue
				if debug:
					print 'target:', t
				# Match is either False or combined name, retaining all info
				match = self.areMatchingNames(q,t,debug=debug)
				if match:
					# If q already has children, just add t as a new child
					if q.children:
						self.mergeNames(q,t)
					else:
						q = self.newGroupNames(q,t)
						q.parts = match

					q.parts = match

					if debug:
						print 'grouped:',q
						putil.dashes()
						print

					ns[i] = None
					ns[index] = q
					index -= 1
					break
			
			if debug:
				print

			index += 1
		return ns
	
	def groupedNamesRecursive(self, ns, index=0, debug=False):
		'''
		This recursive version breaks due to max recursion depth limit, it's also quite inefficient since it generates a list every iteration!
		'''
		if len(ns) > 1 and index+1 < len(ns):
			# First item is query, rest are target names to try matching
			q = ns[index]
			if debug:
				putil.dashes()
				print 'query:', q
				putil.dashes()
			for i in range(index+1,len(ns)):
				t = ns[i]
				if debug:
					print 'target:', t
				# Match is either False or combined name, retaining all info
				match = self.areMatchingNames(q,t,debug=debug)
				if match:
					# If q already has children, just add t as a new child
					if q.children:
						self.mergeNames(q,t)
					else:
						q = self.newGroupNames(q,t)
						q.parts = match
					
					q.parts = match
					
					if debug:
						print 'grouped:',q
						putil.dashes()
						print
						
					return self.groupedNames( ns[:index] + [q] + ns[index+1:i] + ns[i+1:] , index, debug=debug )
			if debug:
				print
			# If nothing was matched this round, move onto next query
			return self.groupedNames( ns , index+1 , debug=debug )
		# When matching is finished (or only 1 item to match)
		return ns
	
	def names(self, verbose=False, debug=False, threshold=0, grouped=False, strict=False, extended=False):
		'''
		This is the main function to retrieve names.
			verbose will output matches and groups
			debug will give more detailed info
			matches are returned with score >= threshold
			grouped will return a grouped list
			extended will allow single words found in the dictionary to be grouped
			strict will ignore matches without a title and a word at the end
		'''
		text = self._text
		self._matches = []
		if not text:
			raise Exception("No text loaded")
		
		if verbose:
			putil.header('NAME FINDER', center=True, dash='=')
		
		if verbose:
			putil.info('%d characters in text' % len(text))
			putil.dashes()
			putil.info('Resources:')
			for i in self._resources:
				r = self._resources[i]
				d = r.resource
				putil.info( '- %d entries in "%s"' % (len(d), r.name), padding=2 )
			putil.dashes()

		namesMatch = re.finditer(self.RE_NAMES_FULL, text, re.UNICODE)
		
		for m in namesMatch:
			i = m.groups()
			prefix = i[0]
			title = i[1]
			initials = i[2]
			parts = re.split(r"\s+", i[3])
			plural = i[4]
			suffix = i[5]
			
			score = 0
			
			# Suffix or other parts can contain Inc. Corp. etc.
			# It can also be a building type like Hall, Church
			badSuffix = ['business', 'buildings', 'bad_suffix']
			for d in badSuffix:
				isBad = False
				if suffix:
					isBad = self.resourceLookup(suffix, d)
					if not isBad and suffix[-1] == 's':
						isBad = self.resourceLookup(suffix[:-1], d)
				if not isBad:
					for i in parts:
						if self.resourceLookup(i, d) or ( i[-1] == 's' and self.resourceLookup(i[:-1], d) ):
							score -= 1
				else:
					score -= 1
			
			# If last part + plural are a word, remove it E.g. Wouldn't
			if plural and self.isBroadWord(parts[-1]+plural):
				if len(parts) > 1:
					parts = parts[:-1]
					plural = None
				else:
					continue
			
			# Convert to list, can have more than one
			if title:
				title = [title]
			
			# Remove the first part after a separator (or start of text) if it's a word and parts has a name somewhere
			# E.g. "And Charlie" then...
			wordAfterSep = (self.isSeperator(prefix) or m.start() == 0)
			if wordAfterSep and not title:
				while len(parts) > 1 and self.isBroadWord(parts[0]) and not (self.isTitle(parts[0]) or self.isAbbrTitle(parts[0])):
					prefix = parts[0]
					parts = parts[1:]
			
			# Title given in "parts" not as abbr. Doctor Watson vs. Dr. Watson
			# TODO Less reliable than Dr. because "General Motors" is a company, so partial match later results and them sum up the score?
			newParts = parts
			changed = False
			for i in range(len(parts)):
				if len(newParts) > 1 and (self.isTitle(parts[i]) or self.isAbbrTitle(parts[i])):
					part = parts[i]
					if not title:
						title = [part]
					else:
						title.append(part)
					newParts = newParts[1:]
					changed = True
				else:
					break
			if changed:
				parts = newParts
			
			# Mr. or Mrs. etc. title given
			if title:
				score += 1
			
			# Turn John R.R Tolkien into John R R Tolkien
			newParts = []
			changed = False
			for p in parts:
				if p.find('.') > 0:
					newParts.extend([x for x in p.split('.') if len(x) > 0])
					changed = True
				else:
					newParts.append(p)
			if changed:
				parts = newParts
			
			# Go through remaining parts and check if any unique names (not words) exist
			# If not, or last two parts are words, it's probably just a bunch of words
			# E.g. Denver Airport Terminal or Bus Lane
			numWords = 0
			numNames = 0
			areWords = []
			for p in parts:
				isWord = len(p) > 1 and self.isBroadWord(p)
				areWords.append(isWord)
				if isWord:
					numWords += 1
				else:
					numNames += 1
			
			# Will ignore only if last part is a word like "James White" and no title given
			if strict and areWords[-1] and not title:
				continue
			
			badPrefix = ['bad_prefix']
			for d in badPrefix:
				isBad = False
				if prefix:
					isBad = self.resourceLookup(prefix, d)
				if isBad:
					score -= 1
			
			# Ignore when no names found, or last two are words and no title
			if not title and (numNames == 0 or ( len(parts) > 2 and areWords[-1] and areWords[-2] )):
				# If extended mode, then also collect words in case they form names (White)
				if extended:
					score -= 1
				else:
					continue
			
			match = Name()
			match.id = m.start()
			match.score = score
			match.prefix = prefix
			match.title = title
			if initials:
				initials = re.findall(r'\w+', initials)
			match.initials = initials
			match.parts = parts
			match.plural = plural
			match.suffix = suffix
			
			self._matches.append(match)
		
		if debug:
			print
			putil.header('ALL MATCHES', center=True, dash='=')
			for i in self._matches:
				print i
			print
		
		# Second pass we group all matches
		if debug:
			print
			putil.header('GROUPING', center=True, dash='=')
			print
		
		# Matches are not grouped
		groupedMatches = self.groupedNames(self._matches, debug=debug)
		results = [x for x in groupedMatches if x is not None and x.score >= threshold]
		
		if verbose:
			print
			putil.header('GROUPING SUMMARY', center=True, dash='=')
			print 'Score) Grouped Name @ Character offset'
			print '\tIndividual Names'
			print '* = score adjusted to ensure > 0 since title given'
			print
		
			for i in results:
				print ('%s%s) %s @ %s') % (i.score, '*' if i.title else '', i.toFullName(self), i.id)
				if i.children:
					for j in i.children:
						print ('\t%s%s) %s @ %s') % (j.score, '*' if j.title else '', j.toFullName(self), j.id)
					print
		
			print
			putil.header('RESULTS', center=True, dash='=')
			putil.info('%d Potential Entity Matches' % len(groupedMatches))
			putil.info('%d Named Entity Matches' % len(results))
			putil.dashes()
			print
		
		if grouped:
			for i in results:
				print i.toFullName(self)
		else:
			# Need to sort them based on character positions
			sortedResults = []
			for i in results:
				if i.children:
					sortedResults.extend(i.children)
				else:
					sortedResults.append(i)
			sortedResults = sorted(sortedResults, key=lambda k: k.id)
			for i in sortedResults:
				print i.toFullName(self)
