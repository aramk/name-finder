Alexey Fyodorovitch Karamazov was the third son of Fyodor Pavlovitch
Karamazov, a land owner well known in our district in his own day, and
still remembered among us owing to his gloomy and tragic death, which
happened thirteen years ago, and which I shall describe in its proper
place. For the present I will only say that this “landowner”—for so we
used to call him, although he hardly spent a day of his life on his own
estate—was a strange type, yet one pretty frequently to be met with, a
type abject and vicious and at the same time senseless. But he was one of
those senseless persons who are very well capable of looking after their
worldly affairs, and, apparently, after nothing else. Fyodor Pavlovitch,
for instance, began with next to nothing; his estate was of the smallest;
he ran to dine at other men’s tables, and fastened on them as a toady, yet
at his death it appeared that he had a hundred thousand roubles in hard
cash. At the same time, he was all his life one of the most senseless,
fantastical fellows in the whole district. I repeat, it was not
stupidity—the majority of these fantastical fellows are shrewd and
intelligent enough—but just senselessness, and a peculiar national form of
it.

“Hold him! Hold him!” he cried, and dashed after Dmitri. Meanwhile Grigory
had got up from the floor, but still seemed stunned. Ivan and Alyosha ran
after their father. In the third room something was heard to fall on the
floor with a ringing crash: it was a large glass vase—not an expensive
one—on a marble pedestal which Dmitri had upset as he ran past it.

“Well, the only thing I can tell you is this,” said Smerdyakov, as though
thinking better of it; “I am here as an old friend and neighbor, and it
would be odd if I didn’t come. On the other hand, Ivan Fyodorovitch sent
me first thing this morning to your brother’s lodging in Lake Street,
without a letter, but with a message to Dmitri Fyodorovitch to go to dine
with him at the restaurant here, in the market-place. I went, but didn’t
find Dmitri Fyodorovitch at home, though it was eight o’clock. ‘He’s been
here, but he is quite gone,’ those were the very words of his landlady.
It’s as though there was an understanding between them. Perhaps at this
moment he is in the restaurant with Ivan Fyodorovitch, for Ivan
Fyodorovitch has not been home to dinner and Fyodor Pavlovitch dined alone
an hour ago, and is gone to lie down. But I beg you most particularly not
to speak of me and of what I have told you, for he’d kill me for nothing
at all.”

Possibly many of the readers of my novel will feel that in reckoning on
such assistance, and being ready to take his bride, so to speak, from the
hands of her protector, Dmitri showed great coarseness and want of
delicacy. I will only observe that Mitya looked upon Grushenka’s past as
something completely over. He looked on that past with infinite pity and
resolved with all the fervor of his passion that when once Grushenka told
him she loved him and would marry him, it would mean the beginning of a
new Grushenka and a new Dmitri, free from every vice. They would forgive
one another and would begin their lives afresh. As for Kuzma Samsonov,
Dmitri looked upon him as a man who had exercised a fateful influence in
that remote past of Grushenka’s, though she had never loved him, and who
was now himself a thing of the past, completely done with, and, so to say,
non-existent. Besides, Mitya hardly looked upon him as a man at all, for
it was known to every one in the town that he was only a shattered wreck,
whose relations with Grushenka had changed their character and were now
simply paternal, and that this had been so for a long time.

I will note once for all that Nikolay Parfenovitch, who had but lately
arrived among us, had from the first felt marked respect for Ippolit
Kirillovitch, our prosecutor, and had become almost his bosom friend. He
was almost the only person who put implicit faith in Ippolit
Kirillovitch’s extraordinary talents as a psychologist and orator and in
the justice of his grievance. He had heard of him in Petersburg. On the
other hand, young Nikolay Parfenovitch was the only person in the whole
world whom our “unappreciated” prosecutor genuinely liked. On their way to
Mokroe they had time to come to an understanding about the present case.
And now as they sat at the table, the sharp-witted junior caught and
interpreted every indication on his senior colleague’s face—half a word, a
glance, or a wink.

“I’ll tell you. This Katya ... Ah! she is a charming, charming creature,
only I never can make out who it is she is in love with. She was with me
some time ago and I couldn’t get anything out of her. Especially as she
won’t talk to me except on the surface now. She is always talking about my
health and nothing else, and she takes up such a tone with me, too. I
simply said to myself, ‘Well, so be it. I don’t care’... Oh, yes. I was
talking of aberration. This doctor has come. You know a doctor has come?
Of course, you know it—the one who discovers madmen. You wrote for him.
No, it wasn’t you, but Katya. It’s all Katya’s doing. Well, you see, a man
may be sitting perfectly sane and suddenly have an aberration. He may be
conscious and know what he is doing and yet be in a state of aberration.
And there’s no doubt that Dmitri Fyodorovitch was suffering from
aberration. They found out about aberration as soon as the law courts were
reformed. It’s all the good effect of the reformed law courts. The doctor
has been here and questioned me about that evening, about the gold mines.
‘How did he seem then?’ he asked me. He must have been in a state of
aberration. He came in shouting, ‘Money, money, three thousand! Give me
three thousand!’ and then went away and immediately did the murder. ‘I
don’t want to murder him,’ he said, and he suddenly went and murdered him.
That’s why they’ll acquit him, because he struggled against it and yet he
murdered him.”

“But he didn’t murder him,” Alyosha interrupted rather sharply. He felt
more and more sick with anxiety and impatience.

“Yes, I know it was that old man Grigory murdered him.”

“Grigory?” cried Alyosha.

“Yes, yes; it was Grigory. He lay as Dmitri Fyodorovitch struck him down,
and then got up, saw the door open, went in and killed Fyodor Pavlovitch.”

“But why, why?”

“Yes, yes. I was telling lies just now. I was lying against my honor and
my conscience, but I wanted to save him, for he has hated and despised me
so!” Katya cried madly. “Oh, he has despised me horribly, he has always
despised me, and do you know, he has despised me from the very moment that
I bowed down to him for that money. I saw that.... I felt it at once at
the time, but for a long time I wouldn’t believe it. How often I have read
it in his eyes, ‘You came of yourself, though.’ Oh, he didn’t understand,
he had no idea why I ran to him, he can suspect nothing but baseness, he
judged me by himself, he thought every one was like himself!” Katya hissed
furiously, in a perfect frenzy. “And he only wanted to marry me, because
I’d inherited a fortune, because of that, because of that! I always
suspected it was because of that! Oh, he is a brute! He was always
convinced that I should be trembling with shame all my life before him,
because I went to him then, and that he had a right to despise me for ever
for it, and so to be superior to me—that’s why he wanted to marry me!
That’s so, that’s all so! I tried to conquer him by my love—a love that
knew no bounds. I even tried to forgive his faithlessness; but he
understood nothing, nothing! How could he understand indeed? He is a
monster! I only received that letter the next evening: it was brought me
from the tavern—and only that morning, only that morning I wanted to
forgive him everything, everything—even his treachery!”

The President and the prosecutor, of course, tried to calm her. I can’t
help thinking that they felt ashamed of taking advantage of her hysteria
and of listening to such avowals. I remember hearing them say to her, “We
understand how hard it is for you; be sure we are able to feel for you,”
and so on, and so on. And yet they dragged the evidence out of the raving,
hysterical woman. She described at last with extraordinary clearness,
which is so often seen, though only for a moment, in such over-wrought
states, how Ivan had been nearly driven out of his mind during the last
two months trying to save “the monster and murderer,” his brother.