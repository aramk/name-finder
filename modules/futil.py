def read(filename, utf8=False):
	fo = open(filename, 'r')
	txt = fo.read()
	fo.close()
	if utf8:
		return unicode(txt, 'utf-8')
	else:
		return txt

def readlines(filename):
	fo = open(filename, 'r')
	lines = fo.readlines()
	fo.close()
	return lines
	
def writelines(filename, it):
	fo = open(filename, 'w')
	for i in it:
		fo.write(i+'\n')
	fo.close()

def lines(filename, trim=True, blank=False, utf8=False):
	fo = open(filename, 'r')
	lines = []
	if utf8:
		lines.append( unicode(fo.readline(), 'utf-8') )
	else:
		lines = fo.readlines()
	ret = []
	for i in lines:
		if trim:
			i = i.strip()
		if not blank and len(i) == 0:
			continue
		ret.append(i)
			
	fo.close()
	return ret