def header(str, dash='-', width=80, center=False):
	dashes = dash * width
	print dashes
	space = ''
	if center:
		space = ' ' * ((width - len(str))/2)
	print '%s%s' % (space, str.upper())
	print dashes
	
def dashes(dash='-', width=80):
	print dash * width

def info(str, padding=1):
	print '%s%s' % ((' ' * padding), str)