# a = ['Jeff' ,'Klaus', 'Peter', 'Skilling']
# b = ['Jeffry' ,'K', 'P' ,'Skilling']
# a = ['John' ,'Tolkien']
# b = ['J' ,'R', 'R' ,'Tolkien']

def partialMatchStringLists(ss1, ss2):
	if len(ss1) >= len(ss2):
		l = ss1
		s = ss2
	else:
		l = ss2
		s = ss1
	# print l
	# print s
	final = []
	for i in range(len(s)):
		si = s[i]
		li = l[i]
		if si == li:
			final.append(si)
		else:
			pm = partialMatchStrings(si,li)
			if pm:
				final.append(pm)
			else:
				# print 'no pm'
				return None
		# print si
		# print li
	final.extend(l[len(s):])
	# print '.'
	return final
	
def partialMatchStrings(ss1, ss2):
	if len(ss1) >= len(ss2):
		l = ss1
		s = ss2
	else:
		l = ss2
		s = ss1
	# print l
	# print s
	if l == s:
		return s
	elif (l.lower().find(s.lower()) == 0):
		return l
	else:
		return None
	
# print partialMatchStringLists(a[:-1], b[:-1])
# print partialMatchStrings('Hum', 'huMan')