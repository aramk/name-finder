def listToSet(lst):
	ret = set()
	[ret.add(i) for i in lst]
	return ret
	
def setToList(st):
	return [i for i in st]
	
def capList(list):
	caps = [x.capitalize() for x in list]
	list.extend(caps)
	return list

def dictValueOrDefault(key, dct, default=None):
	return default if key not in dct else dct[key]
	