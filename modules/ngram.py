import re
class NGramWords:
	parts = None
	ngrams = None
	first = None
	last = None
	def __init__(self, stringOrList, glue=' '):
		if isinstance(stringOrList, str):
			self.parts = self._string.strip(glue)
		elif isinstance(stringOrList, list):
			self.parts = stringOrList
		else:
			raise Exception("Incorrect argument")
		self.ngrams = self.ngrams()
	
	def ngrams(self, n=2, delim='#'):
		# TODO If n > len(parts), it will return longest possible, maybe raise Exception
		parts = [delim] + self.parts + [delim]
		ngram = set()
		
		done = False
		for i in range(len(parts)):
			gram = ()
			for j in range(n):
				# We don't need a tuple for a unigram
				if n > 1:
					gram += (parts[i+j],)
				else:
					gram = parts[i+j]
				if i + j >= len(parts) - 1:
					# Reached the end of parts
					done = True
					break
			ngram.add(gram)
			if not self.first:
				self.first = gram
			if done:
				break
		if not self.last:
			self.last = gram
		return ngram
		
	def compare(self, other):
		score = 0
		for i in self.ngrams:
			if i in other.ngrams:
				score += 1
		return score