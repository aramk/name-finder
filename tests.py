import timeit

setup = '''
import names
TEST_PATH = 'tests/'
CORPUS_PATH = 'corpuses/'
names = names.NameFinder()

# Change test corpus here
names.load(TEST_PATH+'verne-around-the-world-in-80-days.txt')
# names.load(TEST_PATH+'austen-pride-and-prejudice.txt')
# names.load(TEST_PATH+'dostoevsky-the-brothers-karamazov.txt')
# names.load(TEST_PATH+'twain-adventures-of-huckleberry-finn.txt')
# names.load(TEST_PATH+'enron-skilling.txt')

# For full corpuses
# names.load(CORPUS_PATH+'verne-around-the-world-in-80-days.txt')
'''

# For large texts, you should use extended=False
ta = timeit.Timer(stmt="names.names(verbose=True, debug=False, grouped=True, strict=False, extended=False)", setup=setup)
print ta.timeit(1)
