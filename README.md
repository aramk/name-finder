# NameFinder

## About

The aim of the NameFinder is to **match anthroponyms** (names of people) and ignore other proper nouns such as locations, organisations, etc. in a given corpus such as a novel. 

The system also supports grouping variations of the same name under a single entity and using partial matching to complete missing information found in these variations. 

This field of research is called Named Entity Recognition, and we are concerned with matching the subset of person names.

See the [Project Report](https://bitbucket.org/aramk/name-finder/src/5798650add53aec95502d175009c3ac3a94f06e4/docs/project1-report.pdf) for the detailed background, methodology and results of this research project. 

## Usage

I've tested it on Python 2.7.2, but it should run fine on lower versions.
For a quick demo:

    $ python tests.py

For running in your own .py file, use this code:

    import names
    TEST_PATH = 'tests/'
    CORPUS_PATH = 'corpuses/'
    names = names.NameFinder()
    names.load(TEST_PATH+'verne-around-the-world-in-80-days.txt')
    names.names(verbose=True, debug=False, grouped=True, strict=False, extended=False)
    
The script will handle importing everything else. The `tests.py` script also prints the time taken at the end. No external libraries were used, just word lists.

## Test Results

I've also included a set of [test results](https://bitbucket.org/aramk/name-finder/src/5798650add53/tests/effectiveness.xls?at=master) and effectiveness calculations.

## Credits

University of Melbourne, COMP90049 Knowledge Technologies, Semester 1 2012, Project 1  
By Aram Kocharyan - 359867, <http://aramk.com>